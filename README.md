# Logo Switch

## Description

This code adds the functionality for the admin to switch logos in between pages on a wordpress site.

## How it works

### 1. Metabox

> We first use the `get_theme_mod('custom-logo')` function to get the site logo which we will replace.

- In `wp-content/themes/blocksy/functions.php`, We add an action for adding a meta box on the current page.

```bash
add_action('add_meta_boxes', function () {
	add_meta_box('logo_meta_box', 'Upload Logo', 'render_meta_box', 'page', 'advanced', 'high');
}, 10);
```

- This action makes use of the `render_meta_box` function which renders the meta box on each page.

- From the meta box we get the input values for the `replacing logo url`, `width` and `height`.

### 2. Upload Script

- We add a js folder `wp-content/themes/blocksy/js` into which we add a file named `logo-upload.js` which will be used to change open a media upload dialog.
- We attach this script to the page with the `admin_enqueue_scripts` action in `functions.php`.

### 3. Switch Script

- We add another script `logo-swith.js` for changing the logo and replacing it with the replacing logo from `step 1`.
- We attach this script to the page with the `wp_enqueue_scripts` action in `functions.php`.

### 4. Styles

- We add a css file at `wp-content/themes/blocksy/css/logo-switch.css` which will be used to hide the logo before we change it and later show it when we have changed it. This is implemented through usage of the `.default-logo` class (the default class of the `img` component that holds the logo in the Blocksy theme) and a custom class `.default-logo-show`.
- We attach these styles in `functions.php` with the `wp_enqueue_scripts` action and the `load_styles` function.

## How to use it

- While logged as an admin, go to the site and click on a page whose logo you want to change.

- Click `Edit Page` in the top panel.

- At the bottom of the Editing page you see a small form with text boxed for `Upload new logo`, `width`, and `height`.

- You can leave the width and height boxes empty unless you want your logo to have a custom size (Size is in pixels).

- Click the `Upload image` button to upload a logo of your choosing.

- Its url will automatically be filled in the text box.

- Publish your changes and the logo for that page will be the new logo you just chose.

> If you want to revert back to the old logo, go back to the editing screen, delete the url in the textbox and Publish again.

### _Note:_

_This should work on any site using the Blocksy theme.
To work on another theme it would require some adjustments specific to that particular theme._

## Thank You!

### Contributors:

- [Brian Gitego](https://gitlab.com/gitego)
