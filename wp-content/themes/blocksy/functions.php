<?php

/**
 * Blocksy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Blocksy
 */

if (version_compare(PHP_VERSION, '5.7.0', '<')) {
	require get_template_directory() . '/inc/php-fallback.php';
	return;
}
require get_template_directory() . '/inc/init.php';

add_theme_support('custom-logo');

$logo = get_theme_mod('custom_logo');
$image = wp_get_attachment_image_src($logo, 'full');
$custom_logo_url = $image[0];

function load_styles()
{
	wp_register_style('logo-switch', get_template_directory_uri() . '/css/logo-switch.css', array(), false, 'all');
	wp_enqueue_style('logo-switch');
}

/**
 * attach styles
 */
add_action('wp_enqueue_scripts', 'load_styles');

/**
 *
 * Enqueue Admin Scripts and Styles
 *
 */
add_action('admin_enqueue_scripts', function () {
	wp_enqueue_media();

	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_register_script('logo-upload', get_template_directory_uri() . '/js/logo-upload.js', array('jquery', 'media-upload', 'thickbox'));
	wp_enqueue_script('logo-upload');

	wp_localize_script(
		'logo-upload',
		'meta_image',
		array(
			'title' => __('Choose or Upload Media'),
			'button' => __('Use this media'),
		)
	);
	wp_enqueue_script('logo-upload');
});



/**
 *
 * Enqueue Front End Scripts and Styles
 *
 */
add_action('wp_enqueue_scripts', function () {
	global $post;
	global $custom_logo_url;
	$original_image = $custom_logo_url;
	$replaced_image = get_post_meta($post->ID, 'logo_replacing_url', true);
	$image_width = get_post_meta($post->ID, 'logo_image_width', true);
	$image_height = get_post_meta($post->ID, 'logo_image_height', true);

	wp_register_script('logo-switch', get_template_directory_uri() . '/js/logo-switch.js', array('jquery'));
	wp_enqueue_script('logo-switch');

	wp_localize_script(
		'logo-switch',
		'data',
		array(
			'postID' => $post->ID,
			'original_img_url' => $original_image,
			'replacing_image_url' => $replaced_image,
			'logo_image_width' => $image_width,
			'logo_image_height' => $image_height,
		)
	);
});
/**
 *
 * Add Metabox
 *
 */
add_action('add_meta_boxes', function () {
	add_meta_box('logo_meta_box', 'Upload Logo', 'render_meta_box', ['post', 'page'], 'advanced', 'high');
}, 10);

/**
 *
 * Metabox render Function
 *
 */
function render_meta_box($post)
{
	$replaced_image = get_post_meta($post->ID, 'logo_replacing_url', true);
	$image_width = get_post_meta($post->ID, 'logo_image_width', true);
	$image_height = get_post_meta($post->ID, 'logo_image_height', true);
?>
	<p>
		<label for="logo_replacing_url"><?php _e('Upload new logo') ?></label><br>

		<!-- The field that will hold the URL for the file -->
		<input type="url" class="large-text" name="logo_replacing_url" id="logo_replacing_url" value="<?php echo esc_attr($replaced_image); ?>"><br>

		<button style="margin-top: 10px;" type="button" class="button" data-media-uploader-target="#logo_replacing_url"><?php _e('Upload Media', 'myplugin') ?></button>
	</p>

	<p>
		<label for="logo_image_width">Width : </label>
		<input type="text" id="logo_image_width" name="logo_image_width" value="<?php echo esc_attr($image_width); ?>"> &nbsp; ex. 300
	</p>

	<p>
		<label for="logo_image_height">Height : </label>
		<input type="text" id="logo_image_height" name="logo_image_height" value="<?php echo esc_attr($image_height); ?>"> &nbsp; ex. 300
	</p>

	<?php wp_nonce_field('form_metabox_nonce', 'form_metabox_process'); ?>
<?php
}

/**
 *
 * Runs when page or post is saved
 *
 */

add_action('save_post', function ($post_id) {
	global $custom_logo_url;

	if (!isset($_POST['form_metabox_process']) || !wp_verify_nonce($_POST['form_metabox_process'], 'form_metabox_nonce')) {
		return;
	}

	if (!current_user_can('edit_post', $post_id)) {
		return $post_id;
	}

	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return;
	}

	if (isset($_POST['logo_replacing_url'])) {
		update_post_meta($post_id, 'logo_replacing_url', sanitize_text_field($_POST['logo_replacing_url']));
	}
	if (!isset($_POST['logo_replacing_url'])) {
		update_post_meta($post_id, 'logo_replacing_url', sanitize_text_field($custom_logo_url));
	}

	if (isset($_POST['logo_image_width'])) {
		update_post_meta($post_id, 'logo_image_width', sanitize_text_field($_POST['logo_image_width']));
	}

	if (isset($_POST['logo_image_height'])) {
		update_post_meta($post_id, 'logo_image_height', sanitize_text_field($_POST['logo_image_height']));
	}
});
