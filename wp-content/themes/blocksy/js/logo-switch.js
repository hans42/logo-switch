jQuery(document).ready(function ($) {
  "use strict";
  $('img[src="' + data.original_img_url + '"]').each(function () {
    $(this).attr("src", data.replacing_image_url || data.original_img_url);
    $(this).removeAttr("srcset");
    $(this).removeAttr("sizes");

    if (data.logo_image_width > 0) {
      $(this).attr("width", data.logo_image_width);
      $(this).css("maxWidth", data.logo_image_width + "px");
    }

    if (data.logo_image_height > 0) {
      $(this).attr("height", data.logo_image_height);
      $(this).css("height", data.logo_image_height);
    }
  });
  $(".default-logo").addClass("default-logo-show");
});
