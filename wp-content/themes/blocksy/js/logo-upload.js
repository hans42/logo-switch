jQuery(document).ready(function ($) {
  "use strict";
  var metaImageFrame;
  $("body").click(function (e) {
    var btn = e.target;
    if (!btn || !$(btn).attr("data-media-uploader-target")) return;
    var field = $(btn).data("media-uploader-target");
    e.preventDefault();
    metaImageFrame = wp.media.frames.metaImageFrame = wp.media({
      title: meta_image.title,
      button: { text: "Use this Image" },
      multiple: false,
      frame: "post", // <-- this is the important part
      state: "insert",
    });
    metaImageFrame.on("select", function () {
      var media_attachment = metaImageFrame
        .state()
        .get("selection")
        .first()
        .toJSON();
      $(field).val(media_attachment.url);
    });
    metaImageFrame.on("insert", function (selection) {
      var state = metaImageFrame.state();
      selection = selection || state.get("selection");
      if (!selection) return;
      var attachment = selection.first();
      var display = state.display(attachment).toJSON(); // <-- additional properties
      attachment = attachment.toJSON();
      var imgurl = attachment.sizes[display.size].url;
      jQuery(field).val(imgurl);
    });
    metaImageFrame.open();
  });
});
